import {Component, h} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';

@Component({
  tag: 'test-page',
  styleUrl: 'test-page.css'
})
export class TestPage {
  params: any = {};
  session: any = {};

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-menu-button> </ion-menu-button>
          </ion-buttons>
          <ion-title>Test Page</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        Sup
      </ion-content>,
    ];
  }
}
