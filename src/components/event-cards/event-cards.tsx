import {Component, h, Prop, State} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';
import {UserHttpService} from '../../http_services/user.http_service';

@Component({
  tag: 'event-cards',
  styleUrl: 'event-cards.css'
})
export class Events {
  @Prop() event: {organization_id:number, event_id:number, name:string, start_datetime:Date, end_datetime:Date, location:string, description:string};
  params: any = {};
  session: any = {};
  @State() user: any;

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
    this.user = await new UserHttpService().find(
      this.session.user_id
    );
  }

  render() {
    return [
      <ion-content>
        <ion-card href={`#/event_information/?event_id=${this.event.event_id}`}>
          <ion-card-title>{this.event.name}</ion-card-title>
          <ion-card-subtitle>{this.event.location}</ion-card-subtitle>
        </ion-card>
      </ion-content>
    ];
  }
}
