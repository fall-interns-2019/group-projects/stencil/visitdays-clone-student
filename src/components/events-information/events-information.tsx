import {Component, State, h} from '@stencil/core';
import {EventHttpService} from "../../http_services/event.http_service";
import {RouteService} from "../../services/route.service";

@Component({
  tag: 'events-information',
  styleUrl: 'events-information.css'
})
export class EventsInformation {
  params: any;
  @State() event: any;

  async componentWillLoad() {
    this.params = RouteService.params();
    this.event = await new EventHttpService().findBy({
      id: this.params.id,
      organization_id: this.params.organization_id
    });
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot='start'>
            <ion-menu-button> </ion-menu-button>
          </ion-buttons>
          <ion-title>Events</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-card>
        <ion-list lines={"full"}>
          <ion-card-header>
            <ion-card-title>
              Event 1
            </ion-card-title>
            <ion-item>
            <ion-text> Here is a description of event 1 </ion-text>
            </ion-item>
            <ion-card-content>
              <ion-button fill="outline">1:00pm - 2:00pm</ion-button>
              <ion-button fill="outline">3:00pm - 4:00pm</ion-button>
            </ion-card-content>
          </ion-card-header>
        </ion-list>
      </ion-card>
    ]
  }
}

