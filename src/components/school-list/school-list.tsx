import {Component, h, State} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';
import { OrganizationHttpService } from '../../http_services/organization.service';

@Component({
  tag: 'school-list',
  styleUrl: 'school-list.css'
})
export class SchoolList {
  @State() shownSchools: any[];
  params: any = {};
  session: any = {};
  schools: any[];
  searchBar: HTMLIonSearchbarElement;

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
    this.schools = (await new OrganizationHttpService().query({}));
    this.shownSchools = this.schools;
  }

  onSearchBarChanged(event) {
    this.shownSchools = this.schools.filter((school) => {
      return school.name.toLowerCase().match(event.detail.value.toLowerCase());
    })
  }

  renderSchools() {
    return this.shownSchools.map((school) => {
      return [
        <ion-list>
          <ion-item>
            <ion-avatar style={{marginRight: '1rem'}}>
              <img src={school.avatar} alt='none'/>
            </ion-avatar>
            {school.name}
          </ion-item>
        </ion-list>
      ]
    })
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-menu-button></ion-menu-button>
          </ion-buttons>
          <ion-title>Schools Page</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-searchbar ref={(el) => this.searchBar = el}
                     onIonChange={(evt) => this.onSearchBarChanged(evt)}/>,
      <ion-content>
        {this.renderSchools()}
      </ion-content>,
    ];
  }
}
