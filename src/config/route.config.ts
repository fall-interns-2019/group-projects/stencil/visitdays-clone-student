export class RouteConfig {
  static all() {
    return [
      ...RouteConfig.errors(),
      ...RouteConfig.tabs(),
      ...RouteConfig.student(),
    ];
  }


  static student() {
    return [
      {path: 'test', component: 'test-page'},
      {path:'schools', component: 'school-list'},
      {path: 'profile', component: 'profile-page'},
      { path: 'events', component: 'events-page' },
      {path:'events/info', component: 'events-information'},
      {path:'events-modal', component: 'events-modal'},
      {path:'schools', component: 'school-list'},
    ]
  }

  static tabs() {
    return [
      {path: '', component: 'auth-login'},
      {path: 'sign_in', component: 'auth-login'},
      {path: 'signup', component: 'auth-create-account'},
      {path: 'profile', component: 'profile-page'},
    ]
  }

  static errors() {
    return [
      {path: 'errors/server', component: 'errors-server'},
      {path: 'errors/unauthorized', component: 'errors-unauthorized'},
      {path: 'errors/not-found', component: 'errors-not-found'},
    ]
  }
}
