import {CrudHttpService} from "./crud.http_service";

export class OrganizationHttpService extends CrudHttpService {
  constructor() {
    super('organizations');
  }
}
