import {CrudHttpService} from "./crud.http_service";

export class EventTypeHttpService extends CrudHttpService {
  constructor() {
    super('events/event_types');
    this.url = 'http://localhost:3000/event_types';
  }
}
