import {CrudHttpService} from "./crud.http_service";

export class UserHttpService extends CrudHttpService {
  constructor() {
    super('users/users')
  }
}
