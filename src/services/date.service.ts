export class DateService {
  constructor() {}


  static zeroPad(number) {
    if (number < 10) {
      number = '0' + number;
    }
    return number
  }

  static amPm(hour) {
    if(hour > 12) {
      return 'PM'
    } else {
      return 'AM'
    }
  }

  static prettyDate(dateTime) {
    if(['', undefined, null].includes(dateTime)) { return; }
    const date = new Date(dateTime);
    const year = date.getFullYear();
    const month = DateService.zeroPad(date.getMonth()+1);
    const day = DateService.zeroPad(date.getDate());
    const hour = date.getHours();
    const simpleHour = hour > 12 ? (hour - 12) : (hour);
    const minute = DateService.zeroPad(date.getMinutes());
    return `${month}-${day}-${year} ${simpleHour}:${minute} ${DateService.amPm(hour)}`;
  }

  static timestamp(dateTime) {
    return new Date(dateTime).getTime()
  }
}
