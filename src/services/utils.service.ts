export class UtilsService {
  static deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
}
