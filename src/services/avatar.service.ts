export class AvatarService {
  private cache: any = {};

  picture(userProvider: any) {
    let image = '';
    if ((userProvider).user_id === undefined) {
      image = (userProvider).avatar || this.getImage(userProvider.id, userProvider.first_name, userProvider.last_name);
    } else {
      image = (userProvider).picture || this.getImage((
        userProvider).user_id, userProvider.first_name, userProvider.last_name);
    }
    return image;
  }

  teamPicture(team: any) {
    return team.picture || this.getImage(team.id, team.name, null);
  }

  private getImage(userId: string, firstName: string, lastName: string): any {
    const cacheKey = userId + firstName + lastName;
    if (this.cache[cacheKey] !== undefined) {
      return this.cache[cacheKey];
    } else {
      return this.cache[cacheKey] = this.canvas(userId, firstName, lastName).toDataURL();
    }
  }

  initials(firstName, lastName) {
    return `${(firstName || ' ')[0].toUpperCase()}${(lastName || ' ')[0].toUpperCase()}`.trim();
  }

  canvas(userId, firstName, lastName) {
    const size = 400;
    const canvas = window.document.createElement('canvas');
    const context = canvas.getContext('2d');
    canvas.width = size;
    canvas.height = size;
    canvas.style.width = `${size}px`;
    canvas.style.height = `${size}px`;
    context.fillStyle = this.idToColor(userId);
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.font = '150px Poppins';
    context.textAlign = 'center';
    context.fillStyle = '#fff';
    context.imageSmoothingEnabled = true;
    context.fillText(this.initials(firstName, lastName), size / 2, size / 1.6);


    return canvas;
  }

  idToColor(userId) {
    if (!userId) {
      return '#ddd';
    }
    const colors = [
      '#F44336', '#E57373', '#B71B1C', '#E91D63', '#F06292', '#890E4F', '#673AB7',
      '#9576CD', '#311B91', '#03A9F4', '#4FC3F7', '#02579B', '#009488', '#4DB5AC',
      '#4DB5AC', '#4CAE50', '#81C785', '#1B5E20', '#8BC34A', '#AED581', '#33691E',
      '#CDDC39', '#DCE675', '#827717', '#FFC108', '#FFD54F', '#FF6F02', '#FF9802',
      '#FFB74D', '#E55100', '#FF5721', '#FF5721', '#BF360C', '#607D8B', '#90A4AE', '#253238'
    ];
    const hash = this.hashStr(userId);
    const index = hash % colors.length;
    return colors[index];
  }

  hashStr(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      const charCode = str.charCodeAt(i);
      hash += charCode;
    }
    return hash;
  }
}
