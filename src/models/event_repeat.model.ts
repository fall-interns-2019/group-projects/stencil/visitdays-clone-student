import {CrudModel} from './crud.model';

export class EventRepeat extends CrudModel {
  public static className = 'EventRepeat';
  public organization_id: string;
  public last_modified_by_user_id: string;
  public created_by_user_id: string;
  public event_id: string;
  public times_json: string;
}
